﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SampleConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var list = DataContext.GetPatients();
            Console.WriteLine("Name\t\tDoctor");
            foreach (var patient in list)
            {
                Console.Write(patient.Name + "\t\t" + patient.Doctor.Name);
            }
        }
    }
}
