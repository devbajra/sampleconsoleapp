﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SampleConsoleApp
{
    public class DataContext
    {
        public static IList<Patient> GetPatients()
        {
            var json = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data.json"));
            var list = JsonConvert.DeserializeObject<IList<Patient>>(json);
            return list;
        }
    }
}
