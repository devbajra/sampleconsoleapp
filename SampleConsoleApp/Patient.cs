﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SampleConsoleApp
{
    public class Patient
    {
        public string Name { get; set; }
        public Doctor Doctor { get; set; }
    }
}
